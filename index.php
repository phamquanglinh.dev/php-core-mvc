<?php
/**
 * Tính trừu tượng
 * Tính bao đóng
 * Tính đa hình
 * Tính kế thừa
 * Ví dụ : Con chó
 * Thuộc tính : màu lông, độ dài thân, đuôi, mắt, mõm
 * Phương thức : Ăn,sủa,đi,ngồi,...
 */

class Animal
{
    public function __construct($name)
    {

        $this->name = $name;
    }

    private string $name;

    public function getName(): string
    {
        return $this->name;
    }

    function eat(): void
    {
        echo "Ngoam Ngoam";
    }
}

class Dog extends Animal
{

    private int $long;
    private int $age;

    /**
     * @return mixed
     */
    public function getLong(): int
    {
        return $this->long;
    }

    /**
     * @return mixed
     */
    public function getAge(): int
    {
        return $this->age;
    }

    public function eat(): void
    {
        echo "Nhăm nhăm";
    }

    /**
     * @param $name
     * @param $long
     * @param $age
     */
    public function __construct($name, $long, $age)
    {
        parent::__construct($name);
        $this->long = $long;
        $this->age = $age;
    }
}

$theDog = new Dog("Kiki", 120, 1);

$theDog->eat();
