<?php

namespace App\Controller;

use App\models\User;

include "app/models/User.php";

class UserController
{
    private User $user;

    public function __construct()
    {
        $this->user = new User();
    }

    public function getAllUser(): void
    {
        $userCollection = $this->user->getAll();
        $data = $userCollection;
        include "app/views/user-list-view.php";
    }

    public function getUserById($id): void
    {
        $user = $this->user->getById($id);
        include "app/views/user-info.php";
    }
}