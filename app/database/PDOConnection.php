<?php

namespace App\database;

use PDO;

class PDOConnection
{
    private string $host;
    private string $dbname;
    private string $username;
    private string $password;
    public function __construct()
    {
        $this->host = "localhost";
        $this->dbname = "php-mvc-core";
        $this->username = "root";
        $this->password = "";
    }

    public function getConnect(): PDO
    {
        return new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->username, $this->password);
    }

}