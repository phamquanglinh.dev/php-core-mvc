<?php

namespace App\models;
include "app/database/PDOConnection.php";
include "app/dto/UserListDto.php";

use App\database\PDOConnection;
use DateTime;
use PDO;
use UserListDto;

/**
 * @property int $id
 * @property string $name
 * @property int $age
 * @property DateTime $birthday
 * @property string $email
 * @property string $phone
 * @property string $password
 * @property string $remember_token
 */
class User
{
    const Table = "users";
    private PDO $PDO;

    public function __construct()
    {
        $PDOConnection = new PDOConnection();
        $this->PDO = $PDOConnection->getConnect();
    }

    public function getAll(): array
    {
        $data = [];
        $query = "select * from users";
        $stmt = $this->PDO->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute();
        while ($row = $stmt->fetch()) {
            $userListDto = (new UserListDto(
                name: $row['name'],
                email: $row['email'],
                phone: $row['phone'] ?? "Không có"))->toArray();
            $data[] = $userListDto;
        }
        return $data;

    }

    public function insert(array $data)
    {
    }

    public function getById($id)
    {
        $query = "select * from users where id = $id";
        $stmt = $this->PDO->prepare($query);
        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function update(int $id, array $data)
    {
    }

    public function delete($id)
    {
    }
}