<?php
/**
 * @var array $data
 */
?>
<table>
    <thead>
    <tr>
        <th>Họ và tên</th>
        <th>Email</th>
        <th>SĐT</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($data as $row) { ?>
        <tr>
            <td><?= $row["name"] ?></td>
            <td><?= $row["email"] ?></td>
            <td><?= $row["phone"] ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>